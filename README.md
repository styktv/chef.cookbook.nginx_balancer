# Local Balanced Cluster Demo

This example provides clear and concise demonstration of multinode setup using Chef, Vagrant, Kitchen. Difficulty in this setup is mostly related to networking and providing ability for nodes to become aware of its peers. This is usually not an issue in AWS/boto or plain Python provisioners.  I decided to implement using Vagrant and Test-Kitchen as I believe these to be of paramount importance to my Chef experience. The pattern presented here is a collection of best practices.

### Chef

Standard ChefDK installation on Mac was used to test. ChefDK package is available fror download from Chef website. This package also includes all tools required for to run this example with exception of basic Vagrant/VirtualBox which is outside of scope here.

### Provisioner

This example uses 'kitchen-nodes' provisioner. It is wrapper around ChefZero provisioner but with ability to output collected information into json files since Chef server is not available. It is the basis and a requirement to easily obtain peer node IPs.

### Base Image Box

Base box is a standard 64 bit version of Centos7 downloaded directly from CentOS repo:

```
http://cloud.centos.org/centos/7/vagrant/x86_64/images/
```

All deployments share same exactly image.

### Organization

Single master .kitchen.yml file is used to define 1xNginx load balancer and 2xAppNodes along with required VM settings that translate to eventually to Vagrant files as the test is being run.

### Networking

Tried to emulate real world scenario by allowing traffic only through Nginx load balancers. Application nodes are hidden behind the NAT on the Virtual Box subnet with no direct web access.

### Bundler

All required Gem dependencies are specified in Gemfile and will be installed automatically.

### Berkshelf

All required Cookbook dependencies are specified in Berksfile and will be installed automatically.

# Application Setup

Single Nginx acts as a proxy to two application servers.

### Application Servers

Application is a GoLang application living inside of single file

```
files/appnode.go
```

Chef Supermarked GoLang cookbook was used to install and configure Go. Application file is placed on instance in /tmp file, built and placed in /var/www folder for serving.

SystemD is used to serve application. It seemed to be most logical choice as CentOS natively supports it in version 7. Such setup ended up being very easy to create.

During provisioning data files with host and ip information is placed in two locatations:

On Host from where you run the provisioning (inside of cookbook):
```
test/integration/nodes/appnode1-c7.json
test/integration/nodes/appnode2-c7.json
```

On Node so system can resolve once node is being converged/configured.
```
/tmp/kitchen/nodes/appnode1-c7.json
/tmp/kitchen/nodes/appnode2-c7.json
```

### Nginx Server

Nginx reads information from above files and populates /etc/nginx/nginx.conf template with actual IPs for balancing. SELinux entries are relaxed and service is restarted to make sure config is accepted.

### Testing and Verification

Very important part of Test-Kitchen setup is ability to double check the work. A basic implementation has been provided. Serverspec is testing nodes for listening ports 8484 on AppNodes and port 80 on Nginx

# Installation

Make sure you have your Virtual Box, Vagrant and ChefDK configured. Make sure port 8080 not being used on your computer. Clone repo and then:
```
bash go.bat
```
As an indication of success, last step of script is to connect to Nginx 10 times. The output you should see at the end (about 20 minutes to complete) is this:
```
Hi there, I'm serer from appnode1-c7!
Hi there, I'm serer from appnode2-c7!
Hi there, I'm serer from appnode1-c7!
Hi there, I'm serer from appnode2-c7!
Hi there, I'm serer from appnode1-c7!
Hi there, I'm serer from appnode2-c7!
Hi there, I'm serer from appnode1-c7!
Hi there, I'm serer from appnode2-c7!
Hi there, I'm serer from appnode1-c7!
Hi there, I'm serer from appnode2-c7!
```
...proving that basic Round-Robin balancing is working.

# Few notes

* App nodes are running as root. No no no.
* Builds should not happen on the node but rather be server from binary repository where checksum of binary can be noted into an approved and tested deployment manifest.
* Round robin not recommended as no indication of dead nodes. No healthchecks. No weighted load balancing. No session presistance.
* Cookbooks can be further optimized to avoid service restarts.

### Helper Files

Since we're using local Vagrant it would be nice to SSH to a local box to see what is inside... but where does Kitchen create the Vagrant files? Don't have to worry about this, just use the helper files. Running the one liner from root of repo will SSH you directly into the node. Your 3 options are:
```
bash into_app1.sh
bash into_app2.sh
bash into_nginx.sh
```

# License

This code is released under BSD license. Use it, but I take no responsibility if it burns down your data centre.