#
# All rights reserved - Do Not Redistribute
#
# Last Update: 2016.03.31
# Created By: Peter Styk <peter@styk.tv>

include_recipe 'golang'


cookbook_file '/tmp/appnode.go' do
  source 'appnode.go'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end


bash 'compile_go_appnode' do
  user 'root'
  cwd '/tmp'
  code <<-EOH
  /usr/local/go/bin/go build appnode.go
  EOH
end


directory '/var/www' do
  owner 'root'
  group 'root'
  recursive true
end

bash 'publish_app' do
  user 'root'
  cwd '/tmp'
  code <<-EOH
  mv appnode /var/www
  EOH
end


cookbook_file '/etc/systemd/system/default.target.wants/appnode.service' do
  source 'appnode.service'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

bash 'enable_and_start_appnode_service' do
  user 'root'
  cwd '/etc/systemd/system/default.target.wants'
  code <<-EOH
  sudo systemctl enable //etc/systemd/system/default.target.wants/appnode.service
  sudo systemctl start appnode
  EOH
end

