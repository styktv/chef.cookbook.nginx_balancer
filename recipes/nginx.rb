#
# All rights reserved - Do Not Redistribute
#
# Last Update: 2015.03.31
# Created By: Peter Styk <peter@styk.tv>

include_recipe 'nginx'
yum_package 'policycoreutils-python'

#Pull IP's using helper from node files created by kitchen-nodes provisioner
app1 = search_for_nodes("run_list:*nginx_balancer??appnode* AND fqdn:appnode1-c7")
app2 = search_for_nodes("run_list:*nginx_balancer??appnode* AND fqdn:appnode2-c7")

#Add IP's to nginx node context so its available for us to use
node.normal["nginx_balancer_nodes"]["ip_app1"] = app1[0]['ipaddress']
node.normal["nginx_balancer_nodes"]["ip_app2"] = app2[0]['ipaddress']

service "nginx" do
  action :stop
end

template '/etc/nginx/nginx.conf' do
  source 'nginx.conf.erb'
  owner 'root'
  group 'root'
  mode '0644'
end

service "nginx" do
  action :start
end

#produce denied request
#capture denied requests into policy
#apply policy
bash 'nginx_selinux_policy_update' do
  user 'root'
  cwd '/tmp'
  code <<-EOH
    curl http://localhost    
    sudo cat /var/log/audit/audit.log | grep nginx | grep denied | audit2allow -M mynginx
    semodule -i mynginx.pp
  EOH
end

service "nginx" do
  action :restart
end

