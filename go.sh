#install prerequesites
bundle install

#build cluster
kitchen list
kitchen create

#deploy chef cookbook (this cookbook with dependencies)
kitchen list
kitchen converge

#verify, currently simple but serverspec mechanism is implemented and can be extended
kitchen verify

#assuming vagrant really granted 8080 to nginx instance without conflicnts
for i in {1..10}; 
do 
  curl http://localhost:8080; 
  printf "\n"
done
